package main

import (
	"context"
	"encoding/json"
	"errors"
	"net"
	"sync"
	"time"
)

var errUnauthorized = errors.New("unauthorized")

// The 'account' object returned by the webdav-auth server.
type account struct {
	Name string `json:"ftpname"`
	Path string `json:"home"`
	DN   string `json:"dn"`
}

type authClient interface {
	GetAccounts(context.Context) (map[string]*account, error)
	Authenticate(context.Context, string, string) error
}

type getAccountsRequest struct {
	Type string `json:"type"`
}

type authenticateRequest struct {
	Type     string `json:"type"`
	DN       string `json:"dn"`
	Password string `json:"password"`
}

// Client for webdav-auth, speaks a simple line-based JSON
// request/response protocol over a UNIX socket.
type davAuthClient struct {
	socket string
}

func newDAVAuthClient(socket string) *davAuthClient {
	return &davAuthClient{socket: socket}
}

var authRequestTimeout = 5 * time.Second

func (c *davAuthClient) doRequest(ctx context.Context, req, resp interface{}) error {
	cctx, cancel := context.WithTimeout(ctx, authRequestTimeout)
	defer cancel()
	deadline, _ := cctx.Deadline()

	dialer := &net.Dialer{Deadline: deadline}
	conn, err := dialer.Dial("unix", c.socket)
	if err != nil {
		return err
	}
	defer conn.Close()
	conn.SetDeadline(deadline) //nolint:errcheck

	if err := json.NewEncoder(conn).Encode(req); err != nil {
		return err
	}

	return json.NewDecoder(conn).Decode(resp)
}

// GetAccounts returns the list of DAV accounts for the current user
// (the authentication server figures out our user ID by looking at
// socket peer information).
func (c *davAuthClient) GetAccounts(ctx context.Context) (map[string]*account, error) {
	var accounts map[string]*account

	if err := c.doRequest(ctx, &getAccountsRequest{Type: "get_accounts"}, &accounts); err != nil {
		return nil, err
	}

	return accounts, nil
}

// Authenticate a user, identified by its LDAP DN.
func (c *davAuthClient) Authenticate(ctx context.Context, dn, password string) error {
	var status string
	if err := c.doRequest(ctx, &authenticateRequest{
		Type:     "auth",
		DN:       dn,
		Password: password,
	}, &status); err != nil {
		return err
	}
	if status != "ok" {
		return errUnauthorized
	}
	return nil
}

type authCacheKey struct {
	dn, password string
}

var authCacheTTL = 10 * time.Minute

// A client that will cache authentication results for a little while.
// This makes the WebDAV server more responsive.
type cachingAuthClient struct {
	authClient

	mx        sync.Mutex
	authCache map[authCacheKey]time.Time
}

func newCachingAuthClient(ac authClient) authClient {
	return &cachingAuthClient{
		authClient: ac,
		authCache:  make(map[authCacheKey]time.Time),
	}
}

func (c *cachingAuthClient) Authenticate(ctx context.Context, dn, password string) error {
	c.mx.Lock()
	defer c.mx.Unlock()

	key := authCacheKey{dn: dn, password: password}
	now := time.Now()

	if t, ok := c.authCache[key]; ok && t.After(now) {
		return nil
	}

	err := c.authClient.Authenticate(ctx, dn, password)
	if err == nil {
		c.authCache[key] = now.Add(authCacheTTL)
	}
	return err
}
