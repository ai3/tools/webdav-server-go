FROM docker.io/library/golang:1.21 AS build
ADD . /go/src
WORKDIR /go/src
RUN go build -tags netgo -o /webdav-server . && strip /webdav-server

FROM scratch
COPY --from=build /webdav-server /
