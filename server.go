package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/http/fcgi"
	"os"

	"golang.org/x/net/webdav"
)

var (
	authSocketPath = flag.String("auth-socket", "/var/run/authdav/auth", "`path` to the auth socket")
)

func withAuth(ac authClient, path, username, dn string, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		u, p, ok := req.BasicAuth()
		if !ok {
			w.Header().Set("WWW-Authenticate", fmt.Sprintf(`Basic realm="%s"`, path))
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		if u != username {
			log.Printf("unauthorized access to %s by user %s", path, u)
			http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
			return
		}

		if err := ac.Authenticate(req.Context(), dn, p); err != nil {
			log.Printf("authentication failure on %s by user %s - %v", path, u, err)
			http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
			return
		}

		h.ServeHTTP(w, req)
	})
}

func logError(req *http.Request, err error) {
	// Ignore non-interesting errors.
	if err == nil || errors.Is(err, os.ErrExist) || errors.Is(err, os.ErrNotExist) {
		return
	}
	log.Printf("dav: error: %s: %v", req.URL.Path, err)
}

func ServeFCGI(l net.Listener, ac authClient) error {
	log.Printf("starting DAV server for user ID %d", os.Getuid())

	accounts, err := ac.GetAccounts(context.Background())
	if err != nil {
		return fmt.Errorf("fetching accounts: %w", err)
	}
	if len(accounts) == 0 {
		return errors.New("no DAV accounts for this user ID")
	}

	// For every account, create a DAV handler, which will
	// authenticate only the associated user.
	root := http.NewServeMux()
	for path, account := range accounts {
		webdavHandler := &webdav.Handler{
			Prefix:     path,
			FileSystem: webdav.Dir(account.Path),
			LockSystem: webdav.NewMemLS(),
			Logger:     logError,
		}
		handler := withAuth(ac, path, account.Name, account.DN, webdavHandler)

		// Handle the path prefix, and also the root path
		// without a final slash.
		root.Handle(path, handler)
		root.Handle(path+"/", handler)
	}

	// Run the FCGI server.
	return fcgi.Serve(l, root)
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	ac := newCachingAuthClient(newDAVAuthClient(*authSocketPath))

	l, err := net.FileListener(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}

	err = ServeFCGI(l, ac)
	if err != nil && err != fcgi.ErrConnClosed {
		log.Fatal(err)
	}
}
